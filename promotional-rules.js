class PromotionalRules {
  applyTenPercentDiscount(total) {
    return total > 6000 ? Math.round(total - (total * 0.10)) : total;
  }

  applyCheapChairDiscount(items) {
    const chairCount = items.reduce((acc, item) => item.productCode === '001' ? ++acc : acc, 0);

    return items.map(item =>
      item.productCode === '001' && chairCount > 1 ? {...item, price: item.price - 75} : item);
  }
}

module.exports = PromotionalRules;