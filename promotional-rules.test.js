const PromotionalRules = require('./promotional-rules');
const items = require('./data');

describe('ten percent discount', () => {
  describe('when the total is over £60', () => {
    it('should apply a 10% discount', () => {
      const promotionalRules = new PromotionalRules();
      expect(promotionalRules.applyTenPercentDiscount(7420)).toEqual(6678);
    });
  });

  describe('when the total is under £60', () => {
    it('should not apply a 10% discount', () => {
      const promotionalRules = new PromotionalRules();
      expect(promotionalRules.applyTenPercentDiscount(5425)).toEqual(5425);
    });
  });
});


describe('chair discount', () => {
  describe('when 1 is bought', () => {
    it('should not apply the chair discount', () => {
        const promotionalRules = new PromotionalRules();
        const oneChair = [items[0]];
        expect(promotionalRules.applyCheapChairDiscount(oneChair)).toEqual(oneChair);
    });
  });

  describe('when 2 are bought', () => {
    it('should apply the chair discount', () => {
        const promotionalRules = new PromotionalRules();
        const twoChairs = [items[0], items[0]];
        
        const chairModifiedPrice = {...items[0], price: 850}
        const expectedResult = [chairModifiedPrice, chairModifiedPrice];
        
        expect(promotionalRules.applyCheapChairDiscount(twoChairs)).toEqual(expectedResult);
    });
  });
  
  describe('when more than 2 chairs are bought', () => {
    it('should apply the chair discount', () => {
      const promotionalRules = new PromotionalRules();
      const threeChairs = [items[0], items[0], items[0]];
      
      const chairModifiedPrice = {...items[0], price: 850}
      const expectedResult = [chairModifiedPrice, chairModifiedPrice, chairModifiedPrice];
      
      expect(promotionalRules.applyCheapChairDiscount(threeChairs)).toEqual(expectedResult);
    });
  });
  
  describe('when 2 chairs are bought with something else', () => {
    it('should apply the chair discount', () => {
      const promotionalRules = new PromotionalRules();
      const threeChairs = [items[0], items[0], items[1]];
      
      const chairModifiedPrice = {...items[0], price: 850}
      const expectedResult = [chairModifiedPrice, chairModifiedPrice, items[1]];
      
      expect(promotionalRules.applyCheapChairDiscount(threeChairs)).toEqual(expectedResult);
    });
  });
});