const items = require('./data');
const Checkout = require('./checkout');
const PromotionalRules = require('./promotional-rules');

const promotionalRules = new PromotionalRules();
const checkout = new Checkout(promotionalRules);

checkout.scan(items[0]);
checkout.scan(items[2]);
checkout.scan(items[1]);
checkout.scan(items[0]);
checkout.scan(items[2]);
console.log(checkout.total());