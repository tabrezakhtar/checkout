const formatter = require('./currency-formatter');

describe('currency formatting', () => {
  describe('when the amount is 925 for a cheap chair', () => {
    it('should format to £9.25', () => {
      expect(formatter(925)).toEqual('£9.25');
    });
  });

  describe('when the amount is 4500 for a little table', () => {
    it('should format to £45.00', () => {
      expect(formatter(4500)).toEqual('£45.00');
    });
  });

  describe('when the amount is 1995 for a funky light', () => {
    it('should format to £19.95', () => {
      expect(formatter(1995)).toEqual('£19.95');
    });
  });
});