const formatCurrency = require('./currency-formatter');

class Checkout {
  constructor(promotionalRules) {
    this.items = [];
    this.promotionalRules = promotionalRules;
  }

  scan(item) {
    this.items.push(item);
    return item.productCode;
  }

  total() { 
    this.items = this.promotionalRules.applyCheapChairDiscount(this.items);
    const total = this.items.reduce((acc, item) => acc + item.price, 0);
    return formatCurrency(
      this.promotionalRules.applyTenPercentDiscount(total)
    );
  }

  getItems() {
    return this.items;
  }
}

module.exports = Checkout;