const Checkout = require('./checkout');
const PromotionalRules = require('./promotional-rules');
const items = require('./data');

describe('item scanning', () => {
  describe('when an item is scanned', () => {
    it('should return the item code', () => {
        const checkout = new Checkout();
        expect(checkout.scan(items[0])).toEqual('001');
    });
  
    it('should add the item to array', () => {
      const checkout = new Checkout();
      checkout.scan(items[0]);
      expect(checkout.getItems()).toHaveLength(1);
    });
  });
});

describe('checkout totals', () => {
  describe('when the checkout total is called', () => {
    it('should return the total', () => {
        const checkout = new Checkout(new PromotionalRules());
        checkout.scan(items[0]);
        expect(checkout.total()).toEqual('£9.25');
    });
  
    it('should return the total for multiple items', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[1]);
      expect(checkout.total()).toEqual('£54.25');
    });
  });
});

describe('discounts', () => {

  describe('when one chair scanned', () => {
    it('should not add the chair discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      expect(checkout.total()).toEqual('£9.25');
    });
  });

  describe('when two chairs scanned', () => {
    it('should add the chair discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      expect(checkout.total()).toEqual('£17.00');
    });
  });

  describe('when three chairs scanned', () => {
    it('should add the chair discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      expect(checkout.total()).toEqual('£25.50');
    });
  });

  describe('when more than £60 spent', () => {
    it('should add the 10% discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[1]);
      checkout.scan(items[2]);
      expect(checkout.total()).toEqual('£66.78');
    });
  });

  describe('when two chairs added and more than £60 spent', () => {
    it('should add the chair discount and 10% discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      checkout.scan(items[1]);
      checkout.scan(items[1]);
      expect(checkout.total()).toEqual('£96.30');
    });
  });
});

describe('test cases from coding test', () => {

  describe('when all three items scanned', () => {
    it('should add a 10% discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[1]);
      checkout.scan(items[2]);
      expect(checkout.total()).toEqual('£66.78');
    });
  });
  
  describe('when two chairs and light added', () => {
    it('should add a chair discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      checkout.scan(items[2]);
      expect(checkout.total()).toEqual('£36.95');
    });
  });

  describe('when two chairs and over £60', () => {
    it('should add a chair discount', () => {
      const checkout = new Checkout(new PromotionalRules());
      checkout.scan(items[0]);
      checkout.scan(items[0]);
      checkout.scan(items[1]);
      checkout.scan(items[2]);
      expect(checkout.total()).toEqual('£73.76');
    });
  });
});