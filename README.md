# Code test for Vinterior
#### Completed by Tabrez Akhtar

The test is done with JavaScript.  It will require [Node](https://nodejs.org/en/download/package-manager/) to be installed.

To run the app:

`npm install`

`node index`

The output should be:

```
checkout> node index   
£91.71
```

Run unit tests:  
`npm test`

To see the code coverage:  
`npm run coverage`  
open `coverage/lcov-report/index.html` in the browser